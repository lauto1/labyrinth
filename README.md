labyrinth


le projet est un labyrinth classique où les collisions sont interdites. En cas de collision , le curseur revient à sa position de depart où la page refresh.

idées : 

-plusieurs balises sont dispersées sur le chemin, il s'agit de handicapes visuels ou de rotation du plateau de jeu .
-il est egalement possible d'augmenter la taille du curseur .
- certaines balises peuvent activer un mouvement ponctuel ou regulier de certains murs.
